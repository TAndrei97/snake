#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define N 15

void afis(int *a, int n);

void cadru_matrice(int *a, int n);

int get_input(unsigned int timeout);

void deplasare(int *elementPozVeche, int *pozNoua,int pozActuala, char dirVeche, char dirNoua, int dimensiuneMatrice);

void momeala(int *a, int n, int *poz);

int mers_invers(char dirVeche, char dirNoua);

void mv_snake(int *v,int *a, int lungime, int n);

int ecran[N][N];

int main()
{

    char chn = 'd';
    char chv = 'd';
    int i;
    int j;
    int elemVeche;
    int pozNoua;
    int poz;
    int n = 6;
    int k = 0;
    int v[1000];
    int pozMom;
    int lungimeSnake = 3;

    v[0] = (N/2)*100+(N/2)-1;
    v[1] = (N/2)*100+(N/2);
    v[2] = (N/2)*100+(N/2)+1;
    momeala(ecran,N,&pozMom);
    ecran[v[0]/100][v[0]%100] = 196;
    ecran[v[1]/100][v[1]%100] = 196;
    ecran[v[2]/100][v[2]%100] = 233;
    srand(time(NULL));

    while(k == 0)
    {
        system("cls");
        chv = chn;
        cadru_matrice(ecran,N);
        afis(ecran,N);
        chn = get_input(3000);
        if(chn == EOF)
            chn = chv;
        deplasare(&elemVeche, &pozNoua, v[lungimeSnake-1], chv, chn, N);
        if(mers_invers(chv, chn) == 1)
            chn = chv;

        mv_snake(v,ecran,lungimeSnake,N);
        v[lungimeSnake-1] = pozNoua;

        if(ecran[v[lungimeSnake-1]/100][v[lungimeSnake-1]%100] >= 179 && ecran[v[lungimeSnake-1]/100][v[lungimeSnake-1]%100] <= 218) //conditie oprire
            k++;
        if(v[lungimeSnake-1] == pozMom)
        {

            lungimeSnake++;
            v[lungimeSnake-1] = pozMom;


            ecran[v[lungimeSnake-2]/100][v[lungimeSnake-2]%100] = elemVeche;
            ecran[v[lungimeSnake-1]/100][v[lungimeSnake-1]%100] = 233;
            momeala(ecran,N, &pozMom);

        }
        else
        {
            ecran[v[lungimeSnake-1]/100][v[lungimeSnake-1]%100] = 233;
            ecran[v[lungimeSnake-2]/100][v[lungimeSnake-2]%100] = elemVeche;//196 = elemveche
        }

    }
    system("cls");
    printf("Game over\nScore:%d",lungimeSnake-3);

    return 0;
}

void momeala(int *a, int n, int *poz)
{
    int i;
    int j;
    int k = 0;
    srand(time(NULL));
    i = rand()%(n-2)+1;
    j = rand()%(n-2)+1;
    while(k == 0)
    {
        if((a+i*n+j) == 0)
        {
            i = rand()%(n-2)+1;
            j = rand()%(n-2)+1;
        }
        else
            k = 1;
    }
    *(a+i*n+j) = 248;
    *poz = i*100+j;
}

void cadru_matrice(int *a, int n)
{
    int i;
    int j;
    j = n*(n-1);
    for(i = 1; i < n-1; i++)
    {
        *(a+i) = 205;
        *(a+j+i) = 205;
    }
    for(i = 1; i < n-1; i++)
    {
        *(a+i*n) = 186;
        *(a+(i+1)*n-1) = 186;
    }
    *a = 201;
    *(a+n-1) = 187;
    *(a+j) = 200;
    *(a+n*n-1) = 188;
}

void mv_snake(int *v,int *mat, int lungime, int n)
{
    int i;
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;

    e = (*(v))/100;
    f = (*(v))%100;
    *(mat+e*n+f) = 0;
    for(i = 0; i < lungime-1; i++)
    {
        *(v+i) = *(v+i+1);
        a = (*(v+i))/100;
        b = (*(v+i))%100;
        c = (*(v+i+1))/100;
        d = (*(v+i+1))%100;
        *(mat+c*n+d) = *(mat+a*n+b);
    }


}

int mers_invers(char dirVeche, char dirNoua)
{
    if(dirVeche == 'w' && dirNoua == 's')
        return 1;
    if(dirVeche == 's' && dirNoua == 'w')
        return 1;
    if(dirVeche == 'a' && dirNoua == 'd')
        return 1;
    if(dirVeche == 'd' && dirNoua == 'a')
        return 1;
    return 0;
}

void afis(int *a,int n)
{
    int i;
    int j;
    for(i = 0; i < n; i++)
    {

        for(j = 0; j < n; j++)
        {
            printf("%c",*(a+i*n+j));
        }
        printf("\n");
    }
}

int get_input(unsigned int timeout)
{
    for( ; timeout; timeout--)
    {
        if(kbhit())
        {
            return getch();
        }
    }
    return EOF;
}

void deplasare(int *elementPozVeche, int *pozNoua,int pozActuala, char dirVeche, char dirNoua, int dimensiuneMatrice)
{

    int i;
    int j;
    j = pozActuala%100;
    i = (pozActuala/100)%100;
    if(dirVeche == dirNoua)
    {
        if(dirVeche == 'w')
        {
            *elementPozVeche = 179;
            if(i-1 < 1)
                i=dimensiuneMatrice-2;
            else
                i--;
        }
        if(dirVeche == 's')
        {
            *elementPozVeche = 179;
            if(i+1 > dimensiuneMatrice-2)
                i=1;
            else
                i++;
        }
        if(dirVeche == 'a')
        {
            *elementPozVeche = 196;
            if(j-1 <1)
                j = dimensiuneMatrice-2;
            else
                j--;
        }
        if(dirVeche == 'd')
        {
            *elementPozVeche = 196;
            if(j+1 > dimensiuneMatrice-2)
                j=1;
            else
                j++;
        }
    }
    else
    {
        if(dirVeche == 'w')
        {
            if(dirNoua == 's')
            {
                *elementPozVeche = 179;
                if(i-1 < 1)
                    i=dimensiuneMatrice-2;
                else
                    i--;
            }
            if (dirNoua == 'a')
            {
                *elementPozVeche = 191;
                if(j-1 <1)
                    j = dimensiuneMatrice-2;
                else
                    j--;
            }
            if(dirNoua == 'd')
            {
                *elementPozVeche = 218;
                if(j+1 > dimensiuneMatrice-2)
                    j=1;
                else
                    j++;
            }
        }
        if(dirVeche == 's')
        {
            if(dirNoua == 'a')
            {
                *elementPozVeche = 217;
                if(j-1 <1)
                    j = dimensiuneMatrice-2;
                else
                    j--;
            }
            if(dirNoua == 'd')
            {
                *elementPozVeche = 192;
                if(j+1 > dimensiuneMatrice-2)
                    j=1;
                else
                    j++;
            }
            if(dirNoua == 'w')
            {
                *elementPozVeche = 179;
                if(i+1 > dimensiuneMatrice-2)
                    i=1;
                else
                    i++;
            }
        }
        if(dirVeche == 'a')
        {
            if(dirNoua == 'w')
            {
                *elementPozVeche = 192;
                if(i-1 < 1)
                    i=dimensiuneMatrice-2;
                else i--;
            }
            if(dirNoua == 's')
            {
                *elementPozVeche = 218;
                if(i+1 > dimensiuneMatrice-2)
                    i=1;
                else i++;
            }
                if(dirNoua == 'd')
            {
                *elementPozVeche = 196;
                if(j-1 <1)
                    j = dimensiuneMatrice-2;
                else
                    j--;
            }
        }
        if(dirVeche == 'd')
        {
            if(dirNoua == 'w')
            {
                *elementPozVeche = 217;
                if(i-1 < 1)
                    i=dimensiuneMatrice-2;
                else i--;
            }
            if(dirNoua == 's')
            {
                *elementPozVeche = 191;
                if(i+1 > dimensiuneMatrice-2)
                    i=1;
                else
                    i++;
            }
            if(dirNoua == 'a')
            {
                *elementPozVeche = 196;
                if(j+1 > dimensiuneMatrice-2)
                    j=1;
                else
                    j++;
            }
        }
    }
    *pozNoua = i*100+j;
}
